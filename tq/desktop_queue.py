import shutil
import threading
import concurrent.futures


import plyvel


class DesktopQueue:
    def __init__(self, name, create_if_missing):
        self.db = plyvel.DB(name=name, create_if_missing=create_if_missing)
        self.last = int(self.db.get(b"last", 0))
        self.first = int(self.db.get(b"last", 1))
        self.lock = threading.Lock()

    def push(self, item):
        with self.lock:
            self.last += 1
            wb = self.db.write_batch()
            wb.put(b"last", str(self.last).encode())
            wb.put(str(self.last).encode(), item)
            wb.write()

    def pop(self):
        with self.lock:
            item = self.db.get(str(self.first).encode())
            if item is None:
                return None
            wb = self.db.write_batch()
            wb.delete(str(self.first).encode())
            self.first += 1
            wb.put(b"first", str(self.first).encode())
            wb.write()
            return item

    def peek(self):
        with self.lock:
            item = self.db.get(str(self.first).encode())
            return item

    def len(self):
        return self.last - self.first

    def is_empty(self):
        return self.len() > 0

    def close(self):
        self.db.close()


if __name__ == "__main__":

    def producer(q):
        for i in range(1, 3 * 60 * 60 * 9):
            q.push(str(i).encode())

    def consumer(q):
        """
        queue consumer
        """
        while True:
            val = q.pop()
            if val is None:
                break
            if val == "97107".encode():
                # check item in pushed 5 times and pop 5 times
                print(val)

    if __name__ == "__main__":
        db = DesktopQueue("/tmp/testdb/", create_if_missing=True)
        with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
            executor.map(producer, [db, db, db, db, db])
            executor.map(consumer, [db, db, db, db, db])
        print("Completed!")
        db.close()
        shutil.rmtree("/tmp/testdb/", ignore_errors=True)
