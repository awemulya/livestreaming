import base64
import time
import mss
import cv2
import numpy as np
from starlette.applications import Starlette
from starlette.endpoints import WebSocketEndpoint, HTTPEndpoint
from starlette.responses import HTMLResponse, StreamingResponse
from starlette.routing import Route, WebSocketRoute

html_live = """
<html>
  <head>
    <title>Live</title>
  </head>
  <body>
    <h1>Live ...</h1>
    <img src="/live">
  </body>
</html>
"""

html = """
<!DOCTYPE html>
<html>
    <head>
        <title>Live</title>
    </head>
    <body>
        <h1>WebSocket Live</h1>
        <form action="" onsubmit="sendMessage(event)">
            <input type="text" id="messageText" autocomplete="off"/>
            <button>Send</button>
        </form>
        <ul id='messages'>
        </ul>
        <img id ="image" src="">
        <script>
            var ws = new WebSocket("ws://localhost:8000/ws");
            ws.onmessage = function(event) {
                var image = document.getElementById('image')
                image.src = event.data
            };
            function sendMessage(event) {
                var input = document.getElementById("messageText")
                ws.send(input.value)
                input.value = ''
                event.preventDefault()
            }
        </script>
    </body>
</html>
"""
img_data = "data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=="


def generate():
    monitor = {"top": 0, "left": 0, "width": 1280, "height": 720}
    start_time = time.time()
    with mss.mss() as sct:
        while time.time() - start_time < 10:
            grab = sct.grab(monitor)
            img = np.array(grab)
            img = cv2.resize(img, (1280, 720))
            # frame = img
            frame = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            (flag, encodedImage) = cv2.imencode(".jpg", frame)
            if not flag:
                continue
            yield (
                b"--frame\r\n"
                b"Content-Type: image/jpeg\r\n\r\n" + bytearray(encodedImage) + b"\r\n"
            )


class Homepage(HTTPEndpoint):
    async def get(self, request):
        return HTMLResponse(html)


class LivePage(HTTPEndpoint):
    async def get(self, request):
        return HTMLResponse(html_live)


class Live(HTTPEndpoint):
    async def get(self, request):
        generator = generate()
        return StreamingResponse(
            generator, media_type="multipart/x-mixed-replace; boundary=frame"
        )


class Echo(WebSocketEndpoint):
    encoding = "text"

    async def on_receive(self, websocket, data):
        monitor = {"top": 120, "left": 280, "width": 1280, "height": 720}
        start_time = time.time()
        with mss.mss() as sct:
            while time.time() - start_time < 10:
                grab = sct.grab(monitor)
                img = np.array(grab)
                img = cv2.resize(img, (1280, 720))
                # frame = img
                frame = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

                (flag, encodedImage) = cv2.imencode(".jpg", frame)
                if not flag:
                    continue
                img_data = base64.b64encode(encodedImage).decode("utf-8")
                # print(img_data)
                await websocket.send_text(f"data:image/png;base64,{img_data}")


routes = [
    Route("/", Homepage),
    Route("/livepage", LivePage),
    Route("/live", Live),
    WebSocketRoute("/ws", Echo),
]

app = Starlette(routes=routes)
